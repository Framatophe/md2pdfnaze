# md2pdfnaze

Markdown to PDF sans LaTeX tout naze mais pratique.

Pourquoi ?
-------------

Expression du besoin : transformer un fichier
markdown en PDF en utilisant pandoc mais sans utiliser LaTeX.

Être allergique à la ligne de commande mais accepter de copier
et coller des commandes de temps à autre pourvu que ce soit facile.

Solution : utiliser wkhtmltopdf et un modèle CSS pour faire joli.

Pandoc
-----------

Il faut installer pandoc

Wkhtmltopdf
-----------------

Il faut installer wkhtmltopdf

Notez que selon les plateformes il y a un bug 
depuis longtemps : les liens internes du genre
tables des matières, appel de note, etc. ne
rendent pas dans le PDF de sortie : il garde
les liens vers le fichier html de départ.

Mais si on veut un vrai PDF correct, autant
utiliser LaTeX, hein ? 

Ce qu'il faut faire
-------------------------------

Vous écrivez et enregistrez dans un dossier votre document
en markdown.

Vous copiez le fichier `csspourpdf.css` ou `csspourpdfNB.css` 
dans ce même dossier.

Vous ouvrez un terminal et allez dans ce dossier (sous Ubuntu,
vous faites clic-droit et « ouvrir dans un terminal ».

Vous copiez la ligne de commande suivante en remplaçant 
`source.md` par le nom de votre fichier markdown et 
`sortie.pdf` par le nom du fichier PDF que vous voulez à la
sortie.

Notez que ces commandes vont aussi produire un fichier
nommé `hopla.html` ce sera la sortie HTML de votre fichier.

```
pandoc --css csspourpdf.css -s source.md -o hopla.html --toc --number-sections && wkhtmltopdf -B 15 -L 15 -R 15 -T 15 --page-size A4 --default-header hopla.html sortie.pdf
```

Comprendre et changer des trucs
--------------------------------

La feuille de style `csspourpdf.css` est en couleur
La feuille de style `csspourpdfNB.css` est en N&B.
Vous pouvez utiliser celle que vous voulez en changeant
ce nom de fichier.

La commande `--toc` : produira un sommaire. Vous pouvez l'enlever.

La commande `--number-sections` : numérote les sections. Vous pouvez l'enlever.

Les variables `-B 15 -L 15 -R 15 -T 15` : les marges du PDF en
millimètres (ici 15 mm partout). Vous pouvez modifier.

